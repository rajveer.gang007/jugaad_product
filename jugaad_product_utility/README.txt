CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

Introduction
-------------
This module provides product content type having title,description,image and
app purchase link fields and a custom block which show QR code of app purchase
link which will be placed in right sidebar.

INSTALLATION & DEPLOYMENT STEP
------------

1. Install the module as normal, see link for instructions.
   Link: https://www.drupal.org/documentation/install/modules-themes/modules-8\

2. This module uses below package to genrate qa code. 
    https://packagist.org/packages/bacon/bacon-qr-code
    
    composer require bacon/bacon-qr-code

CONFIGURATION
-------------
  1) you may need to place 'Node qrbuy block' in correct region.
  2) add .jugaad-product.node.full class in node full twig to apply module css as
  well.
