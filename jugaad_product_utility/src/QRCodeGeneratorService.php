<?php

namespace Drupal\jugaad_product_utility;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class QRCodeGeneratorService.
 */
class QRCodeGeneratorService {

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new QRCodeGeneratorService object.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Method to return QR code for respective string.
   * 
   * @param type $string
   *   App Purchase url string.
   * 
   * @return mixed
   */
  public function getQRCode($string = NULL) {
    if ($string) {
      try {
        if (class_exists('BaconQrCode\Renderer\ImageRenderer')) {
          $renderer = new \BaconQrCode\Renderer\ImageRenderer(
            new \BaconQrCode\Renderer\RendererStyle\RendererStyle(400),
            new \BaconQrCode\Renderer\Image\SvgImageBackEnd()
          );
          $writer = new \BaconQrCode\Writer($renderer);
          return $writer->writeString($string);
        }
        else {
          return t('You may missing <pre>composer require bacon/bacon-qr-code</pre>. <br/>Please read README.txt file.');
        }
      } catch (Exception $ex) {
        $this->loggerFactory->get('jugaad_product_utility')->error($ex->getMessage());
      }
    }
  }

}
