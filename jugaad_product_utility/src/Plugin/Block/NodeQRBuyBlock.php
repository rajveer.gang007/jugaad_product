<?php

namespace Drupal\jugaad_product_utility\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'NodeQRBuyBlock' block.
 *
 * @Block(
 *  id = "node_qrbuy_block",
 *  admin_label = @Translation("Node qrbuy block"),
 * )
 */
class NodeQRBuyBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $jugaadService;
  protected $currentRouteMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->jugaadService = $container->get('jugaad_product_utility.qr_code_generator');
    $instance->currentRouteMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->currentRouteMatch->getParameter('node');
    $build = [];
    $build['#theme'] = 'jugaad_product_utility_qr_block';
    if ($node) {
      $qrCodeXml = $this->jugaadService->getQRCode($node->get('field_app_purchase_link')->uri);
      $build['#data']['qr_code_svg'] = $qrCodeXml;
    }
    else {
      $build['#data']['qr_code_svg_error'] = t('its not a product url. Please place block in correct product node path.');
    }

    return $build;
  }

}
